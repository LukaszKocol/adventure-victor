﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    private float speed = 15f;
    public float horizontalInput;
    public bool verticalInput;
    private Rigidbody playerRb;
    public float jumpForce = 0.7f;
    public float gravityModifier = 5f;
    public bool isGrounded = true;
    public int jumpLimit = 0;
    public int heighLimit = 20;
    public GameObject projectilePrefab;
    public Animator anim;
    public bool isThrowing = false;


    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        Physics.gravity *= gravityModifier;
        Rigidbody projectileRb = projectilePrefab.GetComponent<Rigidbody>();
        if (projectileRb != null)
            projectileRb.velocity = Vector3.right;
    }

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical") > 0;

        if ((playerRb.velocity.y >= -0.1f))
        {
            if (verticalInput && isGrounded)
            {
                if (jumpLimit > heighLimit)
                {
                    isGrounded = false;
                }

                playerRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                jumpLimit++;

            }
            else if (jumpLimit != 0)
                jumpLimit = heighLimit;
        }

        if (horizontalInput > 0)
            transform.eulerAngles = Vector3.up * 90;
        else if (horizontalInput < 0)
            transform.eulerAngles = Vector3.up * -90;


        if (horizontalInput != 0)
        {
            anim.SetFloat("Speed_f", 1);
            transform.Translate(Vector3.forward * Time.deltaTime * speed * Mathf.Abs(horizontalInput));
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, 2, 92), transform.position.y, transform.position.z);
        }
        else
            anim.SetFloat("Speed_f", 0);

        if (Input.GetKeyDown(KeyCode.Space) && isThrowing == false)
        {
            isThrowing = true;
            anim.SetTrigger("Throw");
            StartCoroutine(ThrowCorutine());
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        jumpLimit = 0;
        isGrounded = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Food")
        {
            ParticleManager.particleManager.pickupParticle.transform.position = other.transform.position + (Vector3.forward * -1);
            ParticleManager.particleManager.pickupParticle.Play();
            Destroy(other.gameObject);
        }
    }

    IEnumerator ThrowCorutine()
    {
        yield return new WaitForSeconds(0.5f);

        if (transform.rotation.y > 0)
            Instantiate(projectilePrefab, transform.position + (Vector3.right * 0.5f) + (Vector3.up * 1f), transform.rotation);
        else if (transform.rotation.y < 0)
            Instantiate(projectilePrefab, transform.position + (Vector3.left * 0.5f) + (Vector3.up * 1f), transform.rotation);

        yield return new WaitForSeconds(0.4f);
        isThrowing = false;
    }
}
