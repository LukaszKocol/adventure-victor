﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceAndRotate : MonoBehaviour
{
    public float rotationSpeed = 1;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ChangeRotationSpeed", 0f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * Time.deltaTime * rotationSpeed);
        transform.Rotate(0, 1, 0);
    }

    void ChangeRotationSpeed()
    {
        rotationSpeed = -rotationSpeed;
    }
}
