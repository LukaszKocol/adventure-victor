﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalPosition : MonoBehaviour
{
    public float x;
    public float y;
    public float z;

    private Vector3 localPos= new Vector3();
    // Update is called once per frame
    void Update()
    {
        localPos.x = x;
        localPos.y = y;
        localPos.z = z;

        transform.localPosition = localPos;
    }
}
