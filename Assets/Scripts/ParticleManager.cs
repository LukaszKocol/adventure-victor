﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    public ParticleSystem pickupParticle;
    public ParticleSystem damageParticle;

    public static ParticleManager particleManager;
    // Start is called before the first frame update
    void Start()
    {
        if (particleManager == null)
        {
            particleManager = this;
        }
    }
}
