﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollisions : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            ParticleManager.particleManager.damageParticle.transform.position = other.transform.position + (Vector3.forward * -1) + Vector3.up;
            ParticleManager.particleManager.damageParticle.Play();
            Destroy(other.gameObject);
        }

        Destroy(gameObject);
    }
}
