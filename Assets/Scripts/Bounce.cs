﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounce : MonoBehaviour
{
    public float rotationSpeed = 1;
    public float speed = 10.0f;
    // Start is called before the first frame update
    void Start()
    {
        this.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        transform.Translate(Vector3.up * Time.deltaTime * rotationSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("trigered");
        if (other.tag == "Player")
        {
            this.enabled = true;

            InvokeRepeating("ChangeRotationSpeed", 0f, 1f);
        }
    }

    void ChangeRotationSpeed()
    {
        rotationSpeed = -rotationSpeed;
    }
}
